package main

import (
	"bytes"
	"strings"
	"testing"
)

func TestPrint(t *testing.T) {
	cases := []struct {
		expr   string
		expect string
		cmd    string
	}{
		{
			expr: "*/15 0 1,15 * 1-5",
			cmd:  "/usr/bin/find",
			expect: `minute        0 15 30 45
hour          0
day of month  1 15
month         1 2 3 4 5 6 7 8 9 10 11 12
day of week   1 2 3 4 5
command       /usr/bin/find
`,
		},
		{
			expr: "*/15 0 1,3,3,3,3,2,15,11 * 1-5",
			cmd:  "/usr/bin/find",
			expect: `minute        0 15 30 45
hour          0
day of month  1 2 3 11 15
month         1 2 3 4 5 6 7 8 9 10 11 12
day of week   1 2 3 4 5
command       /usr/bin/find
`,
		},
	}

	for idx, test := range cases {
		sched, err := parseExpr(strings.Split(test.expr, " "))
		if err != nil {
			t.Errorf("case %d: %s", idx, err)
		}

		buf := new(bytes.Buffer)
		printSchedule(buf, sched, test.cmd)

		result := buf.String()
		if result != test.expect {
			t.Errorf("case %d: result mismatch.\n\n%s", idx, result)
		}
	}
}
