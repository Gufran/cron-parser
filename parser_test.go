package main

import (
	"reflect"
	"strings"
	"testing"
)

func TestParser(t *testing.T) {
	cases := []struct {
		expr   string
		expect *Schedule
	}{
		{
			expr: "*/15 0 1,15 * 1-5",
			expect: &Schedule{
				Minute: []int{0, 15, 30, 45},
				Hour:   []int{0},
				DoM:    []int{1, 15},
				Month:  []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12},
				DoW:    []int{1, 2, 3, 4, 5},
			},
		},
		{
			expr: "0-30/5 0 1,15 * 1-5",
			expect: &Schedule{
				Minute: []int{0, 5, 10, 15, 20, 25, 30},
				Hour:   []int{0},
				DoM:    []int{1, 15},
				Month:  []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12},
				DoW:    []int{1, 2, 3, 4, 5},
			},
		},
		{
			expr: "1-30/5 0 1,15 * 1-5",
			expect: &Schedule{
				Minute: []int{1, 6, 11, 16, 21, 26},
				Hour:   []int{0},
				DoM:    []int{1, 15},
				Month:  []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12},
				DoW:    []int{1, 2, 3, 4, 5},
			},
		},
		{
			expr: "*/10 * 1,15,25 3-7 *",
			expect: &Schedule{
				Minute: []int{0, 10, 20, 30, 40, 50},
				Hour:   []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23},
				DoM:    []int{1, 15, 25},
				Month:  []int{3, 4, 5, 6, 7},
				DoW:    []int{1, 2, 3, 4, 5, 6, 7},
			},
		},
		{
			expr: "* * * * *",
			expect: &Schedule{
				Minute: []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59},
				Hour:   []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23},
				DoM:    []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31},
				Month:  []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12},
				DoW:    []int{1, 2, 3, 4, 5, 6, 7},
			},
		},
	}

	for idx, test := range cases {
		result, err := parseExpr(strings.Split(test.expr, " "))
		if err != nil {
			t.Errorf("case %d: unexpected error: %s", idx, err)
		}

		if !reflect.DeepEqual(result, test.expect) {
			t.Errorf("case %d: unexpected result (%#v)", idx, result)
		}
	}
}
