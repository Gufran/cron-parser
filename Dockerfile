FROM golang:1.16 as build
WORKDIR /app
ADD . .
RUN go build -o cron-parser

FROM scratch
COPY --from=build /app/cron-parser /cron-parser

ENTRYPOINT ["/cron-parser"]