package main

import (
	"fmt"
	"strconv"
	"strings"
	"time"
)

type Schedule struct {
	Minute []int
	Hour   []int
	DoM    []int
	Month  []int
	DoW    []int
}

func mkrange(start, end, step int) []int {
	var result []int
	for ; start <= end; start += step {
		result = append(result, start)
	}

	return result
}

func parseExpr(chunks []string) (*Schedule, error) {
	minutes, err := expandMinutes(chunks[0])
	if err != nil {
		return nil, fmt.Errorf("invalid minutes segment. %s", err)
	}

	hours, err := expandHours(chunks[1])
	if err != nil {
		return nil, fmt.Errorf("invalid hours segment. %s", err)
	}

	dom, err := expandDoM(chunks[2])
	if err != nil {
		return nil, fmt.Errorf("invalid day-of-month segment. %s", err)
	}

	months, err := expandMonth(chunks[3])
	if err != nil {
		return nil, fmt.Errorf("invalid months segment. %s", err)
	}

	dow, err := expandDoW(chunks[4])
	if err != nil {
		return nil, fmt.Errorf("invalid day-of-week segment. %s", err)
	}

	return &Schedule{
		Minute: minutes,
		Hour:   hours,
		DoM:    dom,
		Month:  months,
		DoW:    dow,
	}, nil
}

func expandMinutes(s string) ([]int, error) {
	if s == "*" {
		return mkrange(0, 59, 1), nil
	}

	return expand(s, 59)
}

func expandHours(s string) ([]int, error) {
	if s == "*" {
		return mkrange(0, 23, 1), nil
	}

	return expand(s, 23)
}

func expandDoM(s string) ([]int, error) {
	// TODO: figuring out the day of month is a bit tricky because
	//       we need information from the year and the month segments.
	//       For now we assume current month and year and just rely
	//       of the input being valid. As long as we meet the caller's
	//       expectation the result is good enough.

	now := time.Now().AddDate(0, 1, 0)

	// creating the date with day=0 puts us one day before the starting
	// of the month, that is, the last day of the previous month.
	dt := time.Date(now.Year(), now.Month(), 0, 0, 0, 0, 0, time.UTC)
	if s == "*" {
		return mkrange(1, dt.Day(), 1), nil
	}

	return expand(s, dt.Day())
}

func expandMonth(s string) ([]int, error) {
	if s == "*" {
		return mkrange(1, 12, 1), nil
	}

	return expand(s, 12)
}

func expandDoW(s string) ([]int, error) {
	if s == "*" {
		return mkrange(1, 7, 1), nil
	}

	return expand(s, 7)
}

func strToInt(values ...string) ([]int, error) {
	var result []int

	for _, s := range values {
		n, err := strconv.Atoi(s)
		if err != nil {
			return nil, err
		}

		result = append(result, n)
	}

	return result, nil
}

func expand(s string, max int) ([]int, error) {
	switch {
	case strings.Contains(s, "/") && strings.Contains(s, "-"):
		return expandSteppedRange(s)

	case strings.Contains(s, "/"):
		return expandStep(s, max)

	case strings.Contains(s, "-"):
		return expandRange(s)

	case strings.Contains(s, ","):
		values, err := strToInt(strings.Split(s, ",")...)
		if err != nil {
			return nil, err
		}

		m := map[int]struct{}{}
		for _, v := range values {
			m[v] = struct{}{}
		}

		var dedup []int
		for k := range m {
			dedup = append(dedup, k)
		}

		return dedup, nil
	}

	// Default case when the field does not contain a comma, dash or slash.
	n, err := strconv.Atoi(s)
	if err != nil {
		return nil, err
	}

	return []int{n}, nil
}

func expandSteppedRange(s string) ([]int, error) {
	chunks := strings.Split(s, "/")
	r, s := chunks[0], chunks[1]

	chunks = strings.Split(r, "-")
	beg, end := chunks[0], chunks[1]

	nums, err := strToInt(beg, end, s)
	if err != nil {
		return nil, err
	}

	return mkrange(nums[0], nums[1], nums[2]), nil
}

func expandStep(s string, max int) ([]int, error) {
	chunks := strings.Split(s, "/")
	if len(chunks) != 2 {
		return nil, fmt.Errorf("malformed step. step value must contain exactly two values separate by a slash")
	}

	if chunks[0] != "*" {
		return nil, fmt.Errorf("non standard step values are not supported")
	}

	den, err := strconv.Atoi(chunks[1])
	if err != nil {
		return nil, err
	}

	return mkrange(0, max, den), nil
}

func expandRange(s string) ([]int, error) {
	chunks := strings.Split(s, "-")
	if len(chunks) != 2 {
		return nil, fmt.Errorf("malformed range. range must contain exactly two values separated by a dash (-)")
	}

	ns, err := strToInt(chunks...)
	if err != nil {
		return nil, err
	}

	return mkrange(ns[0], ns[1], 1), nil
}
