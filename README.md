# Cron Parser

A simple command like program to parse a cron expression and print the parsed time values.

## Building & Usage

The program is written in Golang, and requires no external dependency.

A Dockerfile is provided to eliminate any target specific complications during build. You can run the following commands in project root to build a docker image and run the program.

```
# To build
docker build -t cron-parser .

# To run
docker run --rm -it cron-parser "*/15 0 1,15 * 1-5 /usr/bin/find"
```

If you don't have Docker engine installed or would like to build it using your local go toolchain, simply run the following commands in project root:

```
go build -o cron-parser

./cron-parser "*/15 0 1,15 * 1-5 /usr/bin/find"
```

## Testing

Run the full test suite by executing

```
go test -v
```

Or use the following command to run tests in a docker container

```
docker run --rm -it -v $(PWD):/app --workdir /app golang:1.16 go test -v
```