package main

import (
	"fmt"
	"io"
	"log"
	"os"
	"sort"
	"strings"
)

func main() {
	if len(os.Args) != 2 {
		log.Fatalf("Usage: %s <expression> <command>", os.Args[0])
	}

	chunks := strings.Split(os.Args[1], " ")
	if len(chunks) < 6 {
		log.Fatalf("malformed cron expression. expecting exactly 5 fields before command")
	}

	expr, err := parseExpr(chunks[:5])
	if err != nil {
		log.Fatalf("failed to parse cron expression. %s", err)
	}

	printSchedule(os.Stdout, expr, chunks[5])
}

func printLine(w io.Writer, label string, values []int) {
	args := []interface{}{
		fmt.Sprintf("%-13s", label),
	}

	sort.Slice(values, func(i, j int) bool {
		return values[i] < values[j]
	})

	for _, v := range values {
		args = append(args, v)
	}

	_, _ = fmt.Fprintln(w, args...)
}

func printSchedule(w io.Writer, s *Schedule, cmd string) {
	printLine(w, "minute", s.Minute)
	printLine(w, "hour", s.Hour)
	printLine(w, "day of month", s.DoM)
	printLine(w, "month", s.Month)
	printLine(w, "day of week", s.DoW)

	_, _ = fmt.Fprintf(w, "%-13s %s\n", "command", cmd)
}
